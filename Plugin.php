<?php 
namespace jammerxd\blogcategorycolor;

use System\Classes\PluginBase;
use RainLab\Blog\Controllers\Categories as CategoriesController;
use RainLab\Blog\Models\Category as Category;
class Plugin extends PluginBase
{
	
	public function pluginDetails()
    {
        return [
            'name'        => 'blogcategorycolor',
            'description' => 'adds color tag for categories',
            'author'      => 'Josh Menzel',
            'icon'        => 'icon-pencil',
            'homepage'    => ''
        ];
    }
    public function registerComponents()
    {
    }

    public function registerSettings()
    {
    }

    public function boot()
    {
          // Add college and teacher field to Users table
        //BackendUserModel::extend(function($model){
        //    $model->belongsTo['team'] = ['technobrave\team\Models\Team'];            

//        });


		//BackendUserModel::extend(function($model){
		//	$myrules = $model->rules;
		//	$myrules['social_twitter'] = 'required';
		//	$model->rules = $myrules;
		//});
	
         // Add college and teacher field to Users form
         CategoriesController::extendFormFields(function($form, $model, $context){

            if (!$model instanceof Category)
                return;

            $form->addTabFields([
                'color' => [
                    'label'   => 'Color',
                    'type' => 'text'
                ],
				'bg-color' => [
                    'label'   => 'Background Color',
                    'type' => 'text'
                ],
				'icon' => [
					'label' => 'Icon',
					'type' => 'text'
			]
            ]);
        });
    }
}