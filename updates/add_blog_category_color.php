<?php namespace jammerxd\blogcategorycolor\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BlogCategoryColor extends Migration
{

    public function up()
    {
        if (!Schema::hasColumn('rainlab_blog_categories', 'color')) {
            Schema::table('rainlab_blog_categories', function($table)
			{
				$table->string('color')->nullable();
			});
        }
        if (!Schema::hasColumn('rainlab_blog_categories', 'bg-color')) {
            Schema::table('rainlab_blog_categories', function($table)
			{
				$table->string('bg-color')->nullable();
			});
        }
    }

    public function down()
    {
    }

}