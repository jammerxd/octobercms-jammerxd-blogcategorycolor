<?php namespace jammerxd\blogcategorycolor\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BlogCategoryIcon extends Migration
{

    public function up()
    {
        if (!Schema::hasColumn('rainlab_blog_categories', 'icon')) {
            Schema::table('rainlab_blog_categories', function($table)
			{
				$table->string('icon')->nullable();
			});
        }
    }

    public function down()
    {
    }

}